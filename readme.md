prérequis :

//////////////////////////////////////////////////

vérifier que vous avez bien installé ruby sur votre machine

ruby -v

gem update --system

-------------- installation de compass

gem install compass

-> check installation de compass

compass -v

-------------------------------------------

gem list -> voir les paquets installés

gem install susy => https://la-cascade.io/decouvrir-susy/
gem install breakpoint

vérifier que vous avez bien installé nodejs sur votre machine

node -v

//////////////////////////////////////////////

installer gulp --> gulp -v


#########################
                        #
       structure        #
                        #
#########################

- dossier racine : workflow

créer 5 fichiers à la racine

gulpfile.js-> permet d'automatiser les taches
.gitignore -> ignorer les librairies ou autres fichiers
.readme.md -> donne des informations sur le projet
config.rb  -> config compass -> require susy et breakpoint
package.json -> commande npm init qui génère le fichier

dossier interne app/

-js
-scss
-css
-img
-fonts

//////////////////////////////////////////////////

Le dossier bower_components contient les libraires

installation via le bower.json => bower install

command : bower list --paths => voir les libs




